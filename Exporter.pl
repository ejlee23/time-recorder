use LaTeX::Table;
use strict;
use warnings;

sub make_table {
#	$table = LaTeX::Table->new(
#		{	header => $header,
#			data => $times,
#			caption => '$title: $name'
#		}
#	);
	my $title = $_[0];
	my $name = $_[1];
	my ($header_ptr, $times_ptr);
	print "make_table: $title\n $name\n";
}

sub main {

	my $file = $ARGV[0] or die "Input file not found\n";

	open (INPUT, $file) or die "Could not open '$file' $!\n";

	#variables
	my $title = <INPUT>;
	chomp $title;
	my @header = ["Event", "Time", "Date"];
	my $count = 0;
	my @times;
	my $name;

	#read each line of the csv file
	while (my $line = <INPUT>) {
		chomp $line;
		if ($line eq $title) {
			if (defined $name) {
				make_table($title, $name, @header);
			}
			print $line;
			$name = <INPUT>;
			chomp $name;
			print "$name\n";
			$count = 0;			
		}
		else {
			#parse each line into the name, event, time, and date fields
			my ($event, $time, $date) = split "," , $line;
			print "$event was swum in $time on $date\n";
			$times[$count][0] = $event;
			$times[$count][1] = $time;
			$times[$count][2] = $date;
			$count++;
		}
	}
	
	make_table($title, $name, @header);
	
	
	close(INPUT);

}

main();