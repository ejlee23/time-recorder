* # README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo is a collection of java files that may be used for recording swimmers times. The current version of the program uses array lists to store swimmers and their times. This is meant for coaches to utilize to enter their swimmers' times for a given date.

The current version (v1) can store times for swimmers and sort best times for each swimmer by the event. Future versions will allow for coaches to select swimmers for a given event and seed the swimmers by time.

### How do I get set up? ###

The java files need to be compiled and then just call main. All of this can be done in a bash terminal. The system uses Java version 1.7.0

The exporter will use XML and XSL to create pdf documents for each swimmer.